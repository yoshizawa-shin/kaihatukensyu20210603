<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>指定行数×指定列数でテーブルを書いてみよう</title>
  </head>
  <body>
    <h1>指定行数×指定列数でテーブルを書いてみよう</h1>

    <form method='POST' action='loop02.php'>
        <input type='text' name='gyo' value=''>行×
        <input type='text' name='retu' value=''>列のテーブルを生成する<br>
        <input type='submit' name='btn1' value='送信'>
        <input type='reset' name='btn2' value='リセット'><br>

        <table border='1'>
            <?php
                for($y=0; $y < $_POST['gyo']; $y++){
                    echo '<tr>';
                    for($x=0; $x < $_POST['retu']; $x++){
                        if($x % 2 == 0){
                            echo    '<td style="background-color: #f0f8ff;">
                                        テスト1
                                    </td>';
                        }else{
                            echo    '<td style="background-color: #c0c0c0;">
                                        テスト2
                                    </td>';
                        }
                    }
                    '</tr>';
                }
            ?>
        </table>
    </form>
  </body>
</html>
