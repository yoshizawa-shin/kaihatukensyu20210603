<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>指定行数でテーブルを書いてみよう</title>
  </head>
  <body>
    <h1>指定行数でテーブルを書いてみようフォーム</h1>

    <form method='POST' action='loop01.php'>
        <input type='text' name='cols' value=''>行のテーブルを生成する<br>
        <input type='submit' name='btn1' value='送信'>
        <input type='reset' name='btn2' value='リセット'><br>

        <table border='1'>
            <?php
                for($i=0; $i < $_POST['cols']; $i++){
                    //echo   ' <tr><td>阪神</td>
                                //<td>巨人</td>
                                //<td>ヤクルト</td>
                                //<td>中日</td>
                                //<td>広島</td>
                                //<td>DeNA</td>
                            //</tr>';

                    if($i % 2 == 0){
                        echo    '<tr>
                                    <td style="background-color: #f0f8ff;">
                                        白
                                    </td>
                                    <td style="background-color: #f0f8ff;">
                                        白
                                    </td>
                                </tr>';
                    }else{
                        echo    '<tr>
                                    <td style="background-color: #c0c0c0;">
                                        灰色
                                    </td>
                                    <td style="background-color: #c0c0c0;">
                                        灰色
                                    </td>
                                </tr>';
                    }
                }
            ?>
        </table>
    </form>
  </body>
</html>
