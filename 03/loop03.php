<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>指定行数×指定列数でテーブルを書いてみよう</title>
  </head>
  <body>
    <h1>指定行数×指定列数でテーブルを書いてみよう</h1>

    <form method='POST' action='loop03.php'>
        <input type='number' name='gyo' value='1'>行×
        <input type='number' name='retu' value='1'>列のテーブルを生成する<br>
        <input type='submit' name='btn1' value='送信'>
        <input type='reset' name='btn2' value='リセット'><br>
        <table border='1'>
            <?php
                for($y=1; $y < $_POST['gyo']; $y++){
                    echo '<tr>';
                    for($x=1; $x < $_POST['retu']; $x++){
                        echo    '<td>'. $y++.'-'.$x++.'</td>';
                    }
                    '</tr>';

                }
            ?>
        </table>
    </form>
  </body>
</html>
