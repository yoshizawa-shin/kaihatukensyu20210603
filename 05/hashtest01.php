<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>配列</title>
  </head>
  <body>
    <h1>配列</h1>
    <form method='POST' action=''>
        <?php
            $me_data = array(
                'fruit' => 'スイカ',
                'sport'  => '野球',
                'town'   => '横浜',
                'age'   =>  '21',
                'food'  =>  'カレーライス',
                'money' =>  '円',
            );

            echo $me_data['town'].'<br />';
            echo $me_data['sport'].'<br />';
            echo $me_data['money'].'<br />';
            echo $me_data['school'];

            echo '<hr>';

            $me_data['age'] = 25;


            var_dump($me_data);
            echo '<hr>';

            foreach($me_data as $each){
                echo $each . '<br />';

            }

            echo '<hr>';

            foreach ($me_data as $key => $value) {
                echo $key . ':' . $value . '<br / >';
            }

            echo '<hr>';
            
        ?>
        <pre>
            <?php var_dump($me_data); ?>
        </pre>
    </form>
  </body>
</html>
