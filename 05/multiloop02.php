<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>配列</title>
  </head>
  <body>
    <h1>連想配列</h1>
        <?php
            $player01 = array(
                'id' => '11',
                'name'=> '平内龍太',
                'position'=>'投手',
                'form'=>'オーバースロー',
                'year'=>'2021',
            );

            $player02 = array(
                'id' => '64',
                'name'=>'大江竜聖',
                'position'=>'投手',
                'form' =>'サイドスロー',
                'year' =>'2017',
            );

            $player03  = array(
                'id'=>'054',
                'name'=>'直江大輔',
                'position'=>'投手',
                'form'=>'オーバースロー',
                'year'=>'2019',
            );

            $player04  = array(
                'id'=>'90',
                'name'=>'戸田懐生',
                'position'=>'投手',
                'form'=>'オーバースロー',
                'year'=>'2021',
            );

            $players = array($player01,$player02,$player03,$player04);
        ?>
        <table border="1">
            <tr>
                    <th style=text-align:center>背番号</th>
                    <th style=text-align:center>名前</th>
                    <th style=text-align:center>ポジション</th>
                    <th width ='130' style=text-align:center>投球フォーム</th>
                    <th style=text-align:center>入団年</th>
            </tr>

            <?php
                foreach ($players as $each) {
                    echo '<tr><td>' . $each['id']. "番</td>"
                        ."<td>"   . $each['name']."</td>"
                        .'<td>'. $each['position'].'</td>'
                        .'<td>'.$each['form'].'</td>'
                        .'<td>'.$each['year'].'年</td>
                        </tr>';
                }
            ?>
        </table>
    </form>
  </body>
</html>
