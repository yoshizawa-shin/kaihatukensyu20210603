<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>配列</title>
  </head>
  <body>
    <h1>配列</h1>
    <form method='POST' action=''>
    <table>
        <?php
            $team_a = array('菅野','高橋','サンチェス','今村','畠','山崎');
            $team_b = array('高梨','中川','大江','デラロサ','戸根','ビエイラ');
            $team_c = array('大城','炭谷','岸田','小林','山瀬','前田');
            $team_d = array('坂本','岡本','スモーク','吉川','若林','廣岡');
            $team_e = array('ウィーラー','丸','松原','重信','立岡','梶谷');

            $team_all = array($team_a,$team_b,$team_c,$team_d,$team_e);

            echo '<pre>';
            var_dump($team_all);
            echo '</pre>';

            echo '<hr>';
            foreach ($team_all as $each){
                    foreach($each as $aaaa){
                        echo $aaaa;
                }
            echo '<br />';
            }


        ?>
    </table>
    </form>
  </body>
</html>
