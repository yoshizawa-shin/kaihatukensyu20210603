<!DOCTYPE html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>消費税計算ページ</title>
  </head>
  <body>
    <h1>消費税計算ページ</h1>
    <form method='get' action='tax.php'>
        <table border='1' style='border-collapse:collapse;'>
            <tr>
                <th width='150'>
                    商品名
                </th>
                <th width='180'>
                    価格(単位 :円、税抜き)
                </th>
                <th width='100'>
                    個数
                </th>
                <th width='120'>
                    税率
                </th>
            </tr>
            <tr>
                <td width='150'>
                    <input type='text' name='syoname1' value=''>
                </td>
                <td>
                    <input type='text' name='kakaku1' value=''>
                </td>
                <td>
                    <input type='text' size='5' name='kosuu1' value=''>個
                </td>
                <td>
                    <label><input type='radio' name='zeiritu1' value='8' checked>8%</label>
                    <label><input type='radio' name='zeiritu1' value='10'>10%</label>
                </td>
            </tr>
            <tr>
                <td width='150'>
                    <input type='text' name='syoname2' value=''>
                </td>
                <td>
                    <input type='text' name='kakaku2' value=''>
                </td>
                <td>
                    <input type='text' size='5' name='kosuu2' value=''>個
                </td>
                <td>
                    <label><input type='radio' name='zeiritu2' value='8' checked>8%</label>
                    <label ><input type='radio' name='zeiritu2' value='10'>10%</label>
                </td>
            </tr>
            <tr>
                <td width='150'>
                    <input type='text' name='syoname3' value=''>
                </td>
                <td>
                    <input type='text' name='kakaku3' value=''>
                </td>
                <td>
                    <input type='text' size='5' name='kosuu3' value=''>個
                </td>
                <td>
                    <label><input type='radio' name='zeiritu3' value='8' checked>8%</label>
                    <label><input type='radio' name='zeiritu3' value='10'>10%</label>
                </td>
            </tr>
            <tr>
                <td width='150'>
                    <input type='text' name='syoname4' value=''>
                </td>
                <td>
                    <input type='text' name='kakaku4' value=''>
                </td>
                <td>
                    <input type='text' size='5' name='kosuu4' value=''>個
                </td>
                <td>
                    <labe><input type='radio' name='zeiritu4' value='8' checked>8%</label>
                    <label><input type='radio' name='zeiritu4' value='10'>10%</label>
                </td>
            </tr>
            <tr>
                <td width='150'>
                    <input type='text' name='syoname5' value=''>
                </td>
                <td>
                    <input type='text' name='kakaku5' value=''>
                </td>
                <td>
                    <input type='text' size='5' name='kosuu5' value=''>個
                </td>
                <td>
                    <label><input type='radio' name='zeiritu5' value='8' checked>8%</label>
                    <label><input type='radio' name='zeiritu5' value='10'>10%</label>
                </td>
            </tr>
        </table>
            <input type=submit name='btn1' value='送信'>
            <input type=submit name='btn2' value='リセット'>
    </form>
        <table border='1' style='border-collapse:collapse;'>
            <tr>
                <th width='150'>
                    商品名
                </th>
                <th width='180'>
                    価格(単位 :円、税抜き)
                </th>
                <th width='100'>
                    個数
                </th>
                <th width='120'>
                    税率
                </th>
                <th>
                    小計(単位 :円)
                </th>
            </tr>
            <tr>
                <td>
                    <?php echo $_GET['syoname1'];?>
                </td>
                <td>
                    <?php echo $_GET['kakaku1'];?>
                </td>
                <td>
                    <?php echo $_GET['kosuu1'];?>
                </td>
                <td>
                    <?php echo $_GET['zeiritu1'];?>%
                </td>

                <td>
                    <?php
                        $kakaku1 = $_GET['kakaku1'];
                        $kosuu1 = $_GET['kosuu1'];
                        $zeiritu1 = $_GET['zeiritu1'];
                        echo number_format($kakaku1*$kosuu1*(1+($zeiritu1/100)));
                        $kei1 = $kakaku1 * $kosuu1 * (1 + ($zeiritu1 / 100));
                     ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $_GET['syoname2'];?>
                </td>
                <td>
                    <?php echo $_GET['kakaku2'];?>
                </td>
                <td>
                    <?php echo $_GET['kosuu2'];?>
                </td>
                <td>
                    <?php echo $_GET['zeiritu2'];?>%
                </td>

                <td>
                    <?php
                        $kakaku2 = $_GET['kakaku2'];
                        $kosuu2 = $_GET['kosuu2'];
                        $zeiritu2 = $_GET['zeiritu2'];
                        echo number_format($kakaku2*$kosuu2*(1+($zeiritu2/100)));
                        $kei2 = $kakaku2 * $kosuu2 * (1 + ($zeiritu2 / 100));
                     ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $_GET['syoname3'];?>
                </td>
                <td>
                    <?php echo $_GET['kakaku3'];?>
                </td>
                <td>
                    <?php echo $_GET['kosuu3'];?>
                </td>
                <td>
                    <?php echo $_GET['zeiritu3'];?>%
                </td>

                <td>
                    <?php
                        $kakaku3 = $_GET['kakaku3'];
                        $kosuu3 = $_GET['kosuu3'];
                        $zeiritu3 = $_GET['zeiritu3'];
                        echo number_format($kakaku3*$kosuu3*(1+($zeiritu3/100)));
                        $kei3 = $kakaku3 * $kosuu3*(1 + ($zeiritu3 / 100));
                     ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $_GET['syoname4'];?>
                </td>
                <td>
                    <?php echo $_GET['kakaku4'];?>
                </td>
                <td>
                    <?php echo $_GET['kosuu4'];?>
                </td>
                <td>
                    <?php echo $_GET['zeiritu4'];?>%
                </td>

                <td>
                    <?php
                        $kakaku4 = $_GET['kakaku4'];
                        $kosuu4 = $_GET['kosuu4'];
                        $zeiritu4 = $_GET['zeiritu4'];
                        echo number_format($kakaku4 * $kosuu4 * (1 + ($zeiritu4 / 100)));
                        $kei4 = $kakaku4 * $kosuu4 *(1 + ($zeiritu4 / 100));
                     ?>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $_GET['syoname5'];?>
                </td>
                <td>
                    <?php echo $_GET['kakaku5'];?>
                </td>
                <td>
                    <?php echo $_GET['kosuu5'];?>
                </td>
                <td>
                    <?php echo $_GET['zeiritu5'];?>%
                </td>

                <td>
                    <?php
                        $kakaku5 = $_GET['kakaku5'];
                        $kosuu5 = $_GET['kosuu5'];
                        $zeiritu5 = $_GET['zeiritu5'];
                        echo number_format($kakaku5 * $kosuu5 * (1 + ($zeiritu5 / 100)));
                        $kei5 = $kakaku5 * $kosuu5*(1 + ($zeiritu5 / 100));

                     ?>
                </td>
            </tr>
            <tr>
                <td colspan='4'>
                    合計
                </td>
                <td>
                    <?php
                        echo number_format($kei1 + $kei2 + $kei3 + $kei4 + $kei5) ;
                    ?>
                </td>
            </tr>
        </table>
  </body>
</html>
